# Docker image of Fluentd with GELF output plugin

Borrowed from the (mostly) abandoned: [qiiq/docker-fluentd-gelf](https://github.com/qiq/docker-fluentd-gelf)

Adds GELF output to FluentD container so we can pump logs to Graylog

